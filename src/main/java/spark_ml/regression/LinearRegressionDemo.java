package spark_ml.regression;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.feature.StandardScaler;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
/**
 * Using this model of linear regression, we will try and predict the values of total impression, which is
sum total of like, comment, and share that a Facebook post will get depending on feature vector
parameters such as Post Month, Post Weekday, Post Hour, and so on.
The dataset can be downloaded from http://archive.ics.uci.edu/ml/datasets/Facebook+metrics and it has around 500
records. To keep the model simple, we will select only a few columns from the preceding dataset and call
it formattedDF .
 * @author andriy
 *
 */
public class LinearRegressionDemo {

	private static final String REGRESSION_DATASET_FACEBOOK_CSV = "/home/andriy/Documents/metods_books/ai_academy/eclipse-python/spark_ml/data/mllib/regression/dataset_Facebook.csv";
	private static final String TMP_LINREGRESSION_PREDICT_RESULT_CSV = "/tmp/linregression_predict_result.txt";
	private static final String TMP_LINREGRESSION_SPARK_ML_MODEL_PATH = "/tmp/linregression.spark_ml_model";

	public static void main(String[] args) {
			SparkConf sparkConf = new SparkConf().setAppName("JavaLinearRegressionExample");
			sparkConf.setMaster("local[*]");
			try(JavaSparkContext jsc = new JavaSparkContext(sparkConf);
				SparkSession ss = new SparkSession(jsc.sc())) {
				SQLContext sqlContext = ss.sqlContext();
	// Load and parse the data file.
			String datapath = REGRESSION_DATASET_FACEBOOK_CSV;

			List<StructField> structFields = new ArrayList<>();
			structFields.add(new StructField("Page total likes", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Type", DataTypes.StringType, false, Metadata.empty()));
			structFields.add(new StructField("Category", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Post Month", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Post Weekday", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Post Hour", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Paid", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Post Total Reach", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Post Total Impressions", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Engaged Users", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Post Consumers", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Post Consumptions", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Post Impressions by people who have liked your Page", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime Post reach by people who like your Page", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Lifetime People who have liked your Page and engaged with your post", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("comment", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("like", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("share", DataTypes.IntegerType, false, Metadata.empty()));
			structFields.add(new StructField("Total Interactions", DataTypes.IntegerType, false, Metadata.empty()));
			StructType schema = new StructType(structFields.toArray(new StructField[]{}));
			
			Dataset<Row> ds = sqlContext.read()
					.option("delimiter", ";")
					.option("header", "true")
					.schema(schema)
					.csv(datapath);
//					.dropDuplicates();

// To keep the model simple, we will select only a few columns from the preceding dataset and call
//			it formattedDF .
			Dataset<Row> formattedDF = ds.select(ds.col("Page total likes"), 
					ds.col("Category"), 
					ds.col("Post Month"),
					ds.col("Post Weekday"), 
					ds.col("Post Hour"), 
					ds.col("Paid"),
					ds.col("Lifetime People who have liked your Page and engaged with your post"), 
					ds.col("Total Interactions")); 
			
			ds.persist();
			Row columnNames = ds.head();
			formattedDF.show(false);
			
// From the feature vector formattedDF we will split the dataset into two, one being train dataset
// and the other being test dataset
			Dataset<Row>[] data = formattedDF.na().drop().randomSplit(new double[] {0.7, 0.3});
			System.out.println("We have training examples count :: "+
					data[0].count()+" and test examples count ::"+data[1].count());
			String[] featuresCols = formattedDF.drop("Total Interactions").columns();
//	the linear regression model accepts single feature vectors we
//	will use VectorAssembler , a transformer that combines multiple raw features into a single feature vector.
			VectorAssembler vectorAssembler = new VectorAssembler().setInputCols(featuresCols)
					.setOutputCol("rawFeatures");
			StandardScaler scaler = new StandardScaler()
					.setInputCol("rawFeatures").setOutputCol("features").setWithStd(true)
					.setWithMean(false);
//			pass the instance of VectorAssembler to a pipeline along with the instance of LinearRegression 
			LinearRegression lr = new LinearRegression().setLabelCol("Total Interactions")
					.setFeaturesCol("rawFeatures")
					.setMaxIter(10)
					.setRegParam(0.3)
					.setElasticNetParam(0.8);
//			Linear Regression together returns a transformer instance lrPipelin
//			Chain vectorAssembler and lr in a Pipeline
			Pipeline lrPipeline = new Pipeline().setStages(new PipelineStage[]{vectorAssembler, lr});
// The pipeline instance lrPipeline is then trained using the fit() method on the training data. 
// The fit() method returns a PipelineModel on which test data can be tested			
			PipelineModel lrModel = lrPipeline.fit(data[0]);
			// Make predictions.
			try {
				lrModel.save(TMP_LINREGRESSION_SPARK_ML_MODEL_PATH);
			} catch (IOException e) {
				e.printStackTrace();
			}
			PipelineModel lrModelLoaded = PipelineModel.load(TMP_LINREGRESSION_SPARK_ML_MODEL_PATH);
			
			Dataset<Row> lrPredictions = lrModel.transform(data[1]);
			Dataset<Row> lrPredictions2 = lrModelLoaded.transform(data[1]);
//			lrPredictions2.printSchema();
//			lrPredictions2.write()
//			.option("header", "true")
//			.csv(TMP_LINREGRESSION_PREDICT_RESULT_CSV);
			lrPredictions.show(20);
			
		}

	}

}
