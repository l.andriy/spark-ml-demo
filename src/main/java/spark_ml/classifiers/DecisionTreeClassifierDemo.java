package spark_ml.classifiers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.tree.DecisionTree;
import org.apache.spark.mllib.tree.configuration.Strategy;
import org.apache.spark.mllib.tree.model.DecisionTreeModel;
import org.apache.spark.mllib.util.MLUtils;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.ColumnName;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.storage.StorageLevel;

import scala.Tuple2;

public class DecisionTreeClassifierDemo {
	public static void main(String[] args) {
		SparkConf sparkConf = new SparkConf().setAppName("JavaDecisionTreeClassificationExample");
		sparkConf.setMaster("local[*]");
		try(JavaSparkContext jsc = new JavaSparkContext(sparkConf);
			SparkSession ss = new SparkSession(jsc.sc())) {
			SQLContext sqlContext = ss.sqlContext();
// Load and parse the data file.
		String datapath = "/home/andriy/Documents/metods_books/ai_academy/DECISION_TREES/CREDIT_SCORING/CREDIT_SCORING.csv";
		List<StructField> structFields = IntStream.range(1, 24).mapToObj(x -> "X" + x)
		.map(colName -> new StructField(colName, DataTypes.IntegerType, false, Metadata.empty()))
		.collect(Collectors.toList());
		structFields.add(new StructField("Y", DataTypes.IntegerType, false, Metadata.empty()));
		StructType schema = new StructType(structFields.toArray(new StructField[] {}));
		
		Dataset<Row> ds = sqlContext.read()
				.option("delimiter", ";")
				.option("header", "true")
				.schema(schema)
				.csv(datapath)
				.dropDuplicates();
		
		ds.persist();
		Row columnNames = ds.head();
		Dataset<Row> features = ds.drop("Y");
		Dataset<Row> target = ds.select("Y");
		
//		DecisionTreeModel tree = DecisionTree.trainClassifier(input, numClasses, categoricalFeaturesInfo, impurity, maxDepth, maxBins);

//		JavaRDD<String> rdd = jsc.textFile(datapath, 1);
//		rdd.persist(StorageLevel.MEMORY_ONLY());
//		long allNuberOfElements = rdd.count();
//		long uniqueNuberOfElements = rdd.distinct().count();
		ds.printSchema();
//		ds.show(false);

		

//		var targetVar = ds.drop
//		System.out.println("ColumnNames: " + columnNames.(columnNames.length() - 1));
//		System.out.printf("allNuberOfElements=%d, uniqueNuberOfElements=%d \n", allNuberOfElements, uniqueNuberOfElements);
		
		
		JavaRDD<LabeledPoint> data = MLUtils.loadLibSVMFile(jsc.sc(), datapath).toJavaRDD();
		JavaRDD<LabeledPoint>[] splits = data.randomSplit(new double[]{0.7, 0.3});
		JavaRDD<LabeledPoint> trainingData = splits[0];
		JavaRDD<LabeledPoint> testData = splits[1];
		int numClasses = 2;
		Map<Integer, Integer> categoricalFeaturesInfo = new HashMap<>();
		String impurity = "gini";
		int maxDepth = 5;
		int maxBins = 32;
		
		DecisionTreeModel model = DecisionTree.trainClassifier(trainingData, numClasses,categoricalFeaturesInfo, impurity, maxDepth, maxBins);

				// Evaluate model on test instances and compute test error
		JavaPairRDD<Double, Double> predictionAndLabel = testData.mapToPair(p -> new Tuple2<>(model.predict(p.features()), p.label()));
		double testErr = predictionAndLabel.filter(pl -> !pl._1().equals(pl._2())).count() / (double) testData.count();

		System.out.println("Test Error: " + testErr);
		System.out.println("Learned classification tree model:\n" + model.toDebugString());

				// Save and load model
		model.save(jsc.sc(), "target/tmp/myDecisionTreeClassificationModel");
		DecisionTreeModel sameModel = DecisionTreeModel.load(jsc.sc(), "target/tmp/myDecisionTreeClassificationModel");
		
		
		}		
	}
}