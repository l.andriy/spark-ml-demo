rcv1v2 (topics; subsets)

    Source: [DL04b]
    # of classes: 101
    # of data: 3,000 / 3,000 (testing)
    # of features: 47,236
    Files:
        rcv1subset_topics_train_1.svm.bz2
        rcv1subset_topics_train_2.svm.bz2
        rcv1subset_topics_train_3.svm.bz2
        rcv1subset_topics_train_4.svm.bz2
        rcv1subset_topics_train_5.svm.bz2
        rcv1subset_topics_test_1.svm.bz2 (testing)
        rcv1subset_topics_test_2.svm.bz2 (testing)
        rcv1subset_topics_test_3.svm.bz2 (testing)
        rcv1subset_topics_test_4.svm.bz2 (testing)
        rcv1subset_topics_test_5.svm.bz2 (testing)